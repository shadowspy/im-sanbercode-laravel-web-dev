@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
     Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>

<p>Actor {{$cast->nama}} with age {{$cast->umur}} {{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection