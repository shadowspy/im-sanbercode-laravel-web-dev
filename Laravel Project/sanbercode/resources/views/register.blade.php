<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas HTML Form</title>
</head>
<body>
    <form action="/send" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <!-- Name -->
        <p>First name:</p>
        <input type="text" id="fname" name="fname">
        <p>Last name</p>
        <input type="text" id="lname" name="lname">

        <!-- Gender -->
        <p>Gender:</p>
        <input type="radio" id="Male" name="gender" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" id="Female" name="gender" value="Female">
        <label for="Female">Female</label><br>
        <input type="radio" id="Other" name="gender" value="Other">
        <label for="Other">Other</label><br>

        <!-- Nationality -->
        <p>Nationality:</p>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>

        <!-- Language -->
        <p>Languange Spoken:</p>
            <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
            <label for="language1">Bahasa Indonesia</label>
            <input type="checkbox" id="language2" name="language2" value="English">
            <label for="language1">English</label>
            <input type="checkbox" id="language1" name="language1" value="Other">
            <label for="language1">Other</label>

        <!-- Bio -->    
        <p>Bio:</p>
        <textarea id="bio" name="bio" rows="12" cols="40"></textarea>
        <br>
        <button type="submit" value="Sign Up">Sign Up</button>
    </form>
</body>
</html>