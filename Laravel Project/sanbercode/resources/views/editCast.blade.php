@extends('layout.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('sub-title')
     Cast
@endsection

@section('content')


<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" value="{{$cast->nama}}" placeholder="Masukan nama cast">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{$message}}</div>        
    @enderror
    <div class="form-group">
      <label>umur</label>
      <input type="number" class="form-control" name="umur" id="umur" value="{{$cast->umur}}" placeholder="Masukan umur cast">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>        
    @enderror
    <div class="form-group">
        <label>bio</label>
        <textarea class="form-control" name="bio" id="bio"  placeholder="Masukan biodata cast">{{$cast->bio}}</textarea>
      </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>        
    @enderror   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection