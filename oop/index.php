<?php

require('Animal.php');
require('Ape.php');
require('Frog.php');

//Release 0
$sheep = new Animal('shaun');

echo "Nama Hewan             : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki hewan      : " . $sheep->legs  . "<br>"; // 4
echo "Apakah berdarah dingin : " . $sheep->cold_blooded  . "<br>"; // "no"

//Release 1
$ape = new Ape('kera sakti');
$frog = new Frog('buduk');
echo "<br>";
echo "Nama Hewan             : " . $frog->name . "<br>"; // "shaun"
echo "Jumlah kaki hewan      : " . $frog->legs  . "<br>"; // 4
echo "Apakah berdarah dingin : " . $frog->cold_blooded  . "<br>"; // "no"
$frog->jump()  . "<br>";

echo "<br>";
echo "<br>";

echo "Nama Hewan             : " . $ape->name . "<br>"; // "shaun"
echo "Jumlah kaki hewan      : " . $ape->legs  . "<br>"; // 4
echo "Apakah berdarah dingin : " . $ape->cold_blooded  . "<br>"; // "no"
$ape->yell()  . "<br>";
